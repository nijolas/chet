﻿using System;

namespace Chet
{
    public sealed class ReturnExpectationBuilder<TOn, TReturns>
    {
        private readonly Expectation expectation;

        internal ReturnExpectationBuilder(Expectation expectation)
        {
            this.expectation = expectation;
        }

        public ReturnExpectationBuilder<TOn, TReturns> Returns(TReturns returns)
        {
            expectation.SetReturns(returns);
            return this;
        }

        public ReturnExpectationBuilder<TOn, TReturns> Does(Delegate func)
        {
            expectation.SetDoes(func);
            return this;
        }

        public ReturnExpectationBuilder<TOn, TReturns> Repeat(int n)
        {
            if (n < 1) throw new InvalidOperationException("Repeat must be a positive number greater than 0. If you want the method *not* to be called, use IsNotCalled.");
            expectation.SetRepeat(n);
            return this;
        }

        public ReturnExpectationBuilder<TOn, TReturns> Throws(Exception exception)
        {
            expectation.SetThrows(exception);
            return this;
        }

        public ReturnExpectationBuilder<TOn, TReturns> IsNotCalled()
        {
            expectation.SetRepeat(Expectation.REPEAT_NO_CALL);
            return this;
        }
    }
}
