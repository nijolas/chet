﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet.Tests
{
    public interface IFakeTest
    {
        void DoStuffNoArgs();
        void DoStuffWithArgs(string arg1);
        string ReturnStuffNoArgs();
        string ReturnStuffWithArgs(string arg1);

        void Int(int i);
        void Decimal(decimal d);
        void String(string s);
        void Long(long l);
    }
}
