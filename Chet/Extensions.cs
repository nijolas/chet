﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Chet
{
    public static class Extensions
    {
        public static VoidExpectationBuilder<TOn> Stub<TOn>(this TOn obj, Expression<Action<TOn>> expectation)
        {
            return new VoidExpectationBuilder<TOn>(new Expectation(obj, expectation, false));
        }

        public static VoidExpectationBuilder<TOn> Expect<TOn>(this TOn obj, Expression<Action<TOn>> expectation)
        {
            return new VoidExpectationBuilder<TOn>(new Expectation(obj, expectation, true));
        }

        public static ReturnExpectationBuilder<TOn, TReturns> Stub<TOn, TReturns>(this TOn obj, Expression<Func<TOn, TReturns>> expectation)
        {
            return new ReturnExpectationBuilder<TOn, TReturns>(new Expectation(obj, expectation, false));
        }

        public static ReturnExpectationBuilder<TOn, TReturns> Expect<TOn, TReturns>(this TOn obj, Expression<Func<TOn, TReturns>> expectation)
        {
            return new ReturnExpectationBuilder<TOn, TReturns>(new Expectation(obj, expectation, true));
        }

        public static void VerifyExpectations(this object obj)
        {
            var fake = GetInternalFake(obj);
            fake.VerifyExpectations();
        }

        internal static Fake GetInternalFake(this object obj)
        {
            var fakeField = obj.GetType().GetField(Faker.FAKE_FIELD_NAME, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (fakeField == null) throw new InvalidOperationException("Cannot set an expectation on this object as it has not been faked. Call Chet.Faker.Cover<TInterface>() to create a fake.");
            return fakeField.GetValue(obj) as Fake;
        }
    }
}
