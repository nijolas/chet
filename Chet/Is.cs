﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet
{
    public static class Is
    {
        public static T Equal<T>(T equalTo)
        {
            return default(T);
        }

        internal static EqualityConstraint<T> CreateEqual<T>(T equalTo)
        {
            return new EqualityConstraint<T>(equalTo);
        }

        public static T NotNull<T>()
            where T : class
        {
            return default(T);
        }

        internal static NotNullConstraint CreateNotNull()
        {
            return new NotNullConstraint();
        }

        public static T Matching<T>(Predicate<T> predicate)
            where T : class
        {
            return default(T);
        }

        internal static MatchingConstraint CreateMatching(Delegate predicate)
        {
            return new MatchingConstraint(predicate);
        }

        public static T Null<T>()
            where T : class
        {
            return default(T);
        }

        internal static NullConstraint CreateNull()
        {
            return new NullConstraint();
        }

        public static T Any<T>()
            where T : class
        {
            return default(T);
        }
        
        internal static AnyConstraint CreateAny()
        {
            return new AnyConstraint();
        }
    }
}
