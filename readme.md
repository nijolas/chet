# Chet

Chet is a simple, minimal, mocking framework for .NET Core 2.0.

## Getting Started

Install Chet via NuGet:

```
	PM> Install-Package Chet -Version 0.1.0
```

To have Chet mock an interface, simply call `Chet.Faker.Cover<TInterface>()`. You may then set expectations on the mock, and verify those expectations during your assert phase.

```
	[TestMethod]
    public void Validate_ContentRootDoesNotExist_DirectoryCreateIsCalled()
    {
        // arrange
        var config = new IOConfig();
        config.ContentRoot = "content root";

        var ioShimMock = Chet.Faker.Cover<IIOShim>();
        ioShimMock.Expect(m => m.DirectoryExists("content root")).Returns(false);
        ioShimMock.Expect(m => m.CreateDirectory("content root"));

        // act
        config.Validate(ioShimMock);

        // assert
        ioShimMock.VerifyExpectations();
    }
```

### Running the tests

Chet's tests are in the `Chet.Tests` project, and use the Microsoft Unit Testing Framework. Just just (Ctrl+R, A) in Visual Studio to make things happen.

## Usage

### Creating a mock

Chet only supports mocking of interfaces. You can create a mock by calling `Chet.Faker.Cover<TInterface>()`:

```
	var repositoryMock = Chet.Faker.Cover<IRepository>();
```

### Expectations and stubs

Mocks are controlled via the extensions methods provided under the `Chet` namespace:

```
	using Chet;

	...

	// sets an expectation that GetById will be called with a single
	// parameter 1, and when it does will return null
	repositoryMock.Expect(x => x.GetById(1)).Returns(null);
	
	// sets a stub that GetById, if called with a single parameter 2,
	// will return a new Entity with Id 2
	repositoryMock.Stub(x => x.GetById(2)).Returns(new Entity { Id = 2 });

	// used in the Assert phase - will verify that all expectations
	// have been met (stubs do not set expectations)
	repositoryMock.VerifyExpectations();
```

### Repeat

By default, expectations and stubs will behave the same way regardless of how many times they are called, and an expectation is considered met if it has been called at least once. This may be overridden by including a `repeat` count, i.e. how many times the call is expected/stubbed, and what will happen. The order these expectations/stubs are set up in determines the order Chet will execute their configuration:

```
	using Chet;

	...

	// sets an expectation that GetById will be called *twice* with a single
	// parameter 1, and when it does will return null
	repositoryMock.Expect(x => x.GetById(1)).Return(null).Repeat(2);

	// sets an expectation that GetById will be called *once* with a single
	// parameter 1, and when it does will return a new Entity with Id 1
	repositoryMock.Expect(x => x.GetById(1)).Return(new Entity { Id = 1 }).Repeat(1);

	// sets an expectation that GetById will be called *once* with a single
	// parameter 1, and when it does will return null
	repositoryMock.Expect(x => x.GetById(1)).Return(null).Repeat(1);

	...

	repositoryMock.GetById(1); // returns null
	repositoryMock.GetById(1); // returns null
	repositoryMock.GetById(1); // returns new Entity { Id = 2 }
	repositoryMock.GetById(1); // returns null

	// any more calls will cause the expecation to fail
```

### Expecting NOT to call something

Expectations can be configured to ensure a method is *not* called, like so:

```
	using Chet;

	...

	// sets an expectation that GetById will NOT be called
	// with a single parameter 1
	repositoryMock.Expect(x => x.GetById(1)).IsNotCalled();
```

### Constraints

Expecations and stubs will, by default, use values passed into their methods as constraints. You can set more complicated or less restrictive constraints like so:

```
	using Chet;

	...

	// sets an expectation that GetById will be NOT be called with ANY value
	repositoryMock.Expect(x => x.GetById(Is.Any<int>())).IsNotCalled();
	
	// sets an expectation that SaveEntity will be called with an Entity whose
	// Id is 1, and will return true
	repositoryMock.Expect(x => x.SaveEntity(Is.Matching(x => x.Id == 1)).Returns(true).Repeat(1);

	// stubs SaveEntity so that if called with a non null value
	// will return true
	repositoryMock.Expect(x => x.SaveEntity(Is.NotNull()).Returns(true);

	// stubs SaveEntity so that if called with a null value
	// will return false
	repositoryMock.Expect(x => x.SaveEntity(Is.Null()).Returns(false);
```

### Throwing exceptions

Expectations and stubs can be configured to throw exceptions, like so:

```
	using Chet;

	...

	// sets an expectation that GetById will be called with a single
	// parameter 1, and when it does will throw an exception
	repositoryMock.Expect(x => x.GetById(1)).Throws(new InvalidOperationException("Could not find ID 1"));
	
	// sets a stub that GetById, if called with a single parameter 2,
	// will throw an exception
	repositoryMock.Stub(x => x.GetById(2)).Throws(new DivideByZeroException());
```

### Doing work

Expectations and stubs may be configured with a function with a matching signature that will execute when the function is called:

```
	using Chet;

	...

	// sets a stub that when GetById is called with any integer parameter
	// it returns an Entity with a matching Id
	repositoryMock.Stub(x => x.GetById(Is.Any<int>())).Does(id => return new Entity { Id = id });
```

## Authors

* **Nicholas Wilson** - [nijolas](https://bitbucket.org/nijolas)

## License

This project is licensed under the [Creative Commons Attribution-ShareAlike 2.0](https://creativecommons.org/licenses/by-sa/2.0/legalcode) license.

## Acknowledgments

* [RhinoMocks](https://www.hibernatingrhinos.com/oss/rhino-mocks) for syntax inspiration
