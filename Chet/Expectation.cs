﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Chet
{
    public class Expectation
    {
        internal const int REPEAT_ANY = -1;
        internal const int REPEAT_NO_CALL = -2;

        internal object Object
        {
            get;
            private set;
        }

        internal bool Required
        {
            get;
            private set;
        }

        internal Expression Expression
        {
            get;
            private set;
        }

        internal string MethodName
        {
            get;
            private set;
        }

        internal Type ReturnType
        {
            get;
            private set;
        }

        internal Constraint[] Constraints
        {
            get;
            private set;
        }

        internal bool ExpectationMet
        {
            get;
            private set;
        }

        internal object Returns
        {
            get;
            private set;
        }

        internal Delegate Does
        {
            get;
            private set;
        }

        internal int Repeat
        {
            get;
            private set;
        }

        internal Exception Throws
        {
            get;
            private set;
        }

        internal int CallCount
        {
            get;
            private set;
        }

        internal bool IsTransient
        {
            get;
            private set;
        }

        internal Expectation(object obj, Expression expression, bool required)
        {
            Object = obj;
            Expression = expression;
            Required = required;
            Repeat = REPEAT_ANY;

            var lambda = expression as LambdaExpression;
            if (lambda == null) throw new InvalidOperationException("Expected lambda expression as expectation.");

            if (lambda.Body is MethodCallExpression)
            {
                BuildMethodCallExpectation(lambda.Body as MethodCallExpression);
            }
            else if (lambda.Body is MemberExpression)
            {
                var memberExpression = (lambda.Body as MemberExpression);
                if (memberExpression.Member.MemberType != System.Reflection.MemberTypes.Property) throw new InvalidOperationException("Properties are the only type of member that can be faked.");

                BuildPropertyExpectation(memberExpression);
            }

            var internalFake = Object.GetInternalFake();
            internalFake.RegisterExpectation(this);
        }

        internal void SetTransient()
        {
            IsTransient = true;
        }

        private void BuildPropertyExpectation(MemberExpression member)
        {
            var property = (member.Member as PropertyInfo);

            MethodName = $"get_{property.Name}";
            ReturnType = property.PropertyType;
        }

        private void BuildMethodCallExpectation(MethodCallExpression methodCall)
        {
            MethodName = methodCall.Method.Name;
            ReturnType = methodCall.Method.ReturnType;

            // look for constraints
            Constraints = new Constraint[methodCall.Arguments.Count];
            for (var i = 0; i < methodCall.Arguments.Count; i++)
            {
                var argument = methodCall.Arguments[i];

                // look for conversion
                Constraint constraint;
                if (TryUnravelIsCall(argument, out constraint))
                {
                    Constraints[i] = constraint;
                    continue;
                }

                if (TryUnravelConstant(argument, out constraint))
                {
                    Constraints[i] = constraint;
                    continue;
                }
            }
        }

        private bool TryUnravelIsCall(Expression argument, out Constraint constraint)
        {
            var constraintMethod = (argument as MethodCallExpression);
            if (constraintMethod != null && constraintMethod.Method.DeclaringType == typeof(Is))
            {
                var factoryMethod = typeof(Is).GetMethod($"Create{constraintMethod.Method.Name}", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
                var args = new object[constraintMethod.Arguments.Count];

                for (var i = 0; i < args.Length; i++)
                {
                    var argExpression = constraintMethod.Arguments[0];

                    if (argExpression is LambdaExpression)
                    {
                        args[i] = (argExpression as LambdaExpression).Compile();
                    }
                    else if (argExpression is ConstantExpression)
                    {
                        args[i] = (argExpression as ConstantExpression).Value;
                    }
                    else
                    {
                        throw new InvalidOperationException($"Could not decode expression {argExpression}");
                    }
                }

                if (factoryMethod.IsGenericMethodDefinition)
                {
                    var genericParameters = constraintMethod.Method.GetGenericArguments();
                    var specificFactoryMethod = factoryMethod.MakeGenericMethod(genericParameters);
                    constraint = specificFactoryMethod.Invoke(null, args) as Constraint;
                    return true;
                }
                else
                {
                    constraint = factoryMethod.Invoke(null, args) as Constraint;
                    return true;
                }
            }

            constraint = null;
            return false;
        }

        private bool TryUnravelConstant(Expression argument, out Constraint constraint)
        {
            var arg = (argument as ConstantExpression);
            if (arg != null)
            {
                constraint = (arg.Value == null ? Is.CreateAny() as Constraint : Is.CreateEqual(arg.Value) as Constraint);
                return true;
            }

            constraint = null;
            return false;
        }

        internal void SetReturns(object obj)
        {
            if (ReturnType == typeof(void)) throw new InvalidOperationException($"Attemted to return {obj.GetType()} from a void method.");
            if (!ReturnType.IsAssignableFrom(obj.GetType())) throw new InvalidOperationException($"Type of return object {obj.GetType()} doesn't match the method return type {ReturnType}.");
            if (Does != null) throw new InvalidOperationException($"A delegate has already been registered on this expectation - you may only have one of Do, Throws or Returns.");
            if (Throws != null) throw new InvalidOperationException($"An exception has already been registered on this expectation - you may only have one of Do, Throws or Returns.");

            Returns = obj;
        }

        internal void SetDoes(Delegate does)
        {
            if (ReturnType == typeof(void) && does.Method.ReturnType != typeof(void)) throw new InvalidOperationException($"Attemted to return {does.Method.ReturnType} from a void method.");
            if (!ReturnType.IsAssignableFrom(does.Method.ReturnType)) throw new InvalidOperationException($"Type of return object {does.Method.ReturnType} doesn't match the method return type {ReturnType}.");
            if (Returns != null) throw new InvalidOperationException($"A return object has already been registered on this expectation - you may only have one of Do, Throws or Returns.");
            if (Throws != null) throw new InvalidOperationException($"An exception has already been registered on this expectation - you may only have one of Do, Throws or Returns.");

            Does = does;
        }

        internal void SetThrows(Exception exception)
        {
            if (exception == null) throw new InvalidOperationException("Cannot throw a null exception.");
            if (Returns != null) throw new InvalidOperationException("A return object has already been registered on this expectation - you may only have one of Do, Throws or Returns.");
            if (Does != null) throw new InvalidOperationException($"A delegate has already been registered on this expectation - you may only have one of Do, Throws or Returns.");

            Throws = exception;
        }

        internal void SetRepeat(int n)
        {
            Repeat = n;
            if (Repeat == REPEAT_NO_CALL) ExpectationMet = true;
        }

        internal void Verify()
        {
            if (Required && !ExpectationMet)
            {
                throw new ExpectationFailedException();
            }
        }

        internal (bool Handled, object Result) HandleCall(string methodName, object[] args)
        {
            if (methodName == MethodName)
            {
                for (var i = 0; i < args.Length; i++)
                {
                    var arg = args[i];
                    var constraint = Constraints[i];

                    if (!constraint.Matches(arg)) goto failed;
                }

                // call count already met, something else needs to handle it now
                if (IsTransient && CallCount == Repeat)
                {
                    return (false, null);
                }

                ++CallCount;
                ExpectationMet = Repeat == REPEAT_ANY || (CallCount == Repeat);

                if (Repeat == REPEAT_NO_CALL)
                {
                    throw new ExpectationFailedException($"Method {methodName} was called when it was expected not to.");
                }
                else if (Does != null)
                {
                    return (true, Does.DynamicInvoke(args));
                }
                else if (Throws != null)
                {
                    throw Throws;
                }
                else
                {
                    return (true, Returns);
                }
            }

            failed:
            return (false, null);
        }
    }
}
