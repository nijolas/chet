﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet
{
    public class MatchingConstraint : Constraint
    {
        private readonly Delegate predicate;

        public MatchingConstraint(Delegate predicate)
        {
            this.predicate = predicate;
        }

        internal override bool Matches(object arg)
        {
            return (bool)predicate.DynamicInvoke(arg);
        }
    }
}
