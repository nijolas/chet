using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chet.Tests
{
    [TestClass]
    public class FakerTests
    {
        [TestMethod]
        public void Cover_CreatesValidFake()
        {
            Faker.Cover<IFakeTest>();
        }

        [TestMethod]
        public void Cover_VoidWithNoArgs_CallSucceeds()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.DoStuffNoArgs();
        }

        [TestMethod]
        public void Cover_VoidWithArgs_CallSucceeds()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.DoStuffWithArgs("hello");
        }
    }
}
