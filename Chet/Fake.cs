﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Chet
{
    public class Fake
    {
        private readonly List<Expectation> expectations;

        public Fake()
        {
            expectations = new List<Expectation>();
        }

        public void HandleVoidCall(string methodName, object[] args)
        {
            HandleCallInternal(methodName, args);
        }

        public object HandleReturnCall(string methodName, object[] args)
        {
            (var handled, var result) = HandleCallInternal(methodName, args);
            if (handled) return result;

            throw new ReturnValueUnhandledException($"Call to {methodName} went unhandled, and therefore could not return a result.");
        }

        private (bool Handled, object Result) HandleCallInternal(string methodName, object[] args)
        {
            for (var i = 0; i < expectations.Count; i++)
            {
                var expectation = expectations[i];
                (var handled, var result) = expectation.HandleCall(methodName, args);
                if (handled) return (handled, result);
            }

            return (false, null);
        }

        internal void RegisterExpectation(Expectation expectationBuilder)
        {
            var existing = this.expectations.Where(e => e.MethodName == expectationBuilder.MethodName);
            if (existing.Count() > 0)
            {
                foreach (var e in existing)
                {
                    if (e.Repeat < 0)
                        throw new InvalidOperationException($"You cannot set up multiple expectations on {expectationBuilder.MethodName} unless you set Repeat counts.");
                    e.SetTransient();
                }
            }

            this.expectations.Add(expectationBuilder);
        }

        internal void VerifyExpectations()
        {
            this.expectations.ForEach(e => e.Verify());
        }
    }
}
