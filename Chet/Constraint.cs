﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet
{
    public abstract class Constraint
    {
        internal abstract bool Matches(object arg);
    }
}
