﻿namespace Chet
{
    public class NullConstraint : Constraint
    {
        internal override bool Matches(object arg)
        {
            return (arg == null);
        }
    }
}
