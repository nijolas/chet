﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chet.Tests
{
    [TestClass]
    public class ConstraintsTest
    {
        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Is_Int_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Int(Is.Equal(1)));

            // act
            fake.Int(2);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Is_Int_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Int(Is.Equal(1)));

            // act
            fake.Int(1);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Value_Int_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Int(1));

            // act
            fake.Int(2);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Value_Int_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Int(1));

            // act
            fake.Int(1);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Is_Decimal_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Decimal(Is.Equal(1m)));

            // act
            fake.Decimal(2m);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Is_Decimal_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Decimal(Is.Equal(1m)));

            // act
            fake.Decimal(1m);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Value_Decimal_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Decimal(1m));

            // act
            fake.Decimal(2m);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Value_Decimal_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Decimal(1m));

            // act
            fake.Decimal(1m);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Is_String_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.String(Is.Equal("test")));

            // act
            fake.String("test2");

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Is_String_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.String(Is.Equal("test")));

            // act
            fake.String("test");

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Value_String_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.String("test"));

            // act
            fake.String("test2");

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Value_String_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.String("test"));

            // act
            fake.String("test");

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Is_Long_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Long(Is.Equal(1L)));

            // act
            fake.Long(2L);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Is_Long_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Long(Is.Equal(1L)));

            // act
            fake.Long(1L);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Value_Long_ConstraintDoesNotMatch_DoesNotVerify()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Long(1L));

            // act
            fake.Long(2L);

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Value_Long_ConstraintDoesMatch_Verifies()
        {
            // arrange
            var fake = Chet.Faker.Cover<IFakeTest>();
            fake.Expect(x => x.Long(1L));

            // act
            fake.Long(1L);

            // assert
            fake.VerifyExpectations();
        }
    }
}
