﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chet.Tests
{
    [TestClass]
    public class ExtensionsTests
    {
        [TestMethod]
        public void Expect_DoesNotThrow()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Expect_ExcpectationNotMet_Throws()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs());
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Expect_ExcpectationMet_DoesNotThrow()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs());
            fake.DoStuffNoArgs();
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Stub_ExcpectationNotMet_DoesNotThrow()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.Stub(x => x.DoStuffNoArgs());
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Stub_ExcpectationMet_DoesNotThrow()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.Stub(x => x.DoStuffNoArgs());
            fake.DoStuffNoArgs();
            fake.VerifyExpectations();
        }
    }
}
