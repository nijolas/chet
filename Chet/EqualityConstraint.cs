﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet
{
    public class EqualityConstraint<T> : Constraint
    {
        private readonly T equalTo;

        public EqualityConstraint(T equalTo)
        {
            this.equalTo = equalTo;
        }

        internal override bool Matches(object arg)
        {
            return Equals(equalTo, arg);
        }
    }
}
