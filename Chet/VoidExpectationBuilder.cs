﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet
{
    public sealed class VoidExpectationBuilder<TOn>
    {
        private readonly Expectation expectation;

        internal VoidExpectationBuilder(Expectation expectation)
        {
            this.expectation = expectation;
        }

        public VoidExpectationBuilder<TOn> Does(Delegate action)
        {
            expectation.SetDoes(action);
            return this;
        }

        public VoidExpectationBuilder<TOn> Repeat(int n)
        {
            if (n < 1) throw new InvalidOperationException("Repeat must be a positive number greater than 0. If you want the method *not* to be called, use IsNotCalled.");
            expectation.SetRepeat(n);
            return this;
        }

        public VoidExpectationBuilder<TOn> Throws(Exception exception)
        {
            expectation.SetThrows(exception);
            return this;
        }

        public VoidExpectationBuilder<TOn> IsNotCalled()
        {
            expectation.SetRepeat(Expectation.REPEAT_NO_CALL);
            return this;
        }
    }
}
