﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chet
{
    [Serializable]
    public class ReturnValueUnhandledException : Exception
    {
        public ReturnValueUnhandledException() { }
        public ReturnValueUnhandledException(string message) : base(message) { }
        public ReturnValueUnhandledException(string message, Exception inner) : base(message, inner) { }
        protected ReturnValueUnhandledException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
