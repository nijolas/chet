﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Chet
{
    public static class Faker
    {
        private static ModuleBuilder moduleBuilder;
        private static Dictionary<Type, Type> fakeTypes;

        internal const string FAKE_FIELD_NAME = "fake";

        static Faker()
        {
            fakeTypes = new Dictionary<Type, Type>();

            var assemblyName = new AssemblyName("__FakerAssembly");
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndCollect);
            moduleBuilder = assemblyBuilder.DefineDynamicModule("__FakerModule");
        }

        public static TInterface Cover<TInterface>()
        {
            if (!fakeTypes.ContainsKey(typeof(TInterface)))
            {
                BuildFakeType<TInterface>();
            }

            return (TInterface)Activator.CreateInstance(fakeTypes[typeof(TInterface)]);
        }

        private static void BuildFakeType<TInterface>()
        {
            var typeBuilder = moduleBuilder.DefineType($"__Faker{typeof(TInterface).Name}", TypeAttributes.Public);
            typeBuilder.AddInterfaceImplementation(typeof(TInterface));

            var fakeField = BuildFakeInternals(typeBuilder);

            foreach (var method in typeof(TInterface).GetMethods())
            {
                BuildFakeMethod(typeBuilder, method, fakeField);
            }

            fakeTypes.Add(typeof(TInterface), typeBuilder.CreateType());
        }

        private static FieldBuilder BuildFakeInternals(TypeBuilder typeBuilder)
        {
            var fakeField = typeBuilder.DefineField(FAKE_FIELD_NAME, typeof(Fake), FieldAttributes.Private | FieldAttributes.InitOnly);

            var constructor = typeBuilder.DefineConstructor(MethodAttributes.Public | MethodAttributes.HideBySig, CallingConventions.HasThis, Type.EmptyTypes);
            var ilGenerator = constructor.GetILGenerator();

            // this.fake = new Fake();
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Newobj, typeof(Fake).GetConstructor(Type.EmptyTypes));
            ilGenerator.Emit(OpCodes.Stfld, fakeField);
            ilGenerator.Emit(OpCodes.Ret);

            return fakeField;
        }

        private static void BuildFakeMethod(TypeBuilder typeBuilder, MethodInfo method, FieldBuilder fakeField)
        {
            var parameterTypes = method.GetParameters().Select(x => x.ParameterType).ToArray();
            var methodBuilder = typeBuilder.DefineMethod(method.Name, MethodAttributes.Public | MethodAttributes.Virtual, method.ReturnType, parameterTypes);
            
            if (method.IsGenericMethodDefinition)
            {
                var genericArguments = method.GetGenericArguments();
                var genericArgumentBuilders = methodBuilder.DefineGenericParameters(genericArguments.Select(x => x.Name).ToArray());

                for (var i = 0; i < genericArguments.Length; i++)
                {
                    var baseType = genericArguments[i].BaseType;
                    genericArgumentBuilders[i].SetBaseTypeConstraint(baseType);

                    var interfaces = genericArguments[i].GetInterfaces();
                    genericArgumentBuilders[i].SetInterfaceConstraints(interfaces);
                }
            }

            var ilGenerator = methodBuilder.GetILGenerator();

            ilGenerator.DeclareLocal(typeof(object[]));
            if (methodBuilder.ReturnType != typeof(void))
            {
                methodBuilder.SetReturnType(method.ReturnType);
                ilGenerator.DeclareLocal(method.ReturnType);
            }
            

            // object[] argsArray = new object[parameterTypes.Length];
            ilGenerator.Emit(OpCodes.Ldc_I4, parameterTypes.Length);
            ilGenerator.Emit(OpCodes.Newarr, typeof(object));
            ilGenerator.Emit(OpCodes.Stloc_0);

            for (int i = 0; i < parameterTypes.Length; i++)
            {
                // argsArray[0] = args[0]
                ilGenerator.Emit(OpCodes.Ldloc_0);
                ilGenerator.Emit(OpCodes.Ldc_I4, i);
                ilGenerator.Emit(OpCodes.Ldarg, i + 1); // skip 'this' which is arg 0

                // value types need boxing
                if (parameterTypes[i].IsValueType) ilGenerator.Emit(OpCodes.Box, parameterTypes[i]);

                ilGenerator.Emit(OpCodes.Stelem_Ref, typeof(object));
            }

            // return this.fake.[HandleVoidCall/HandleReturnCall](methodName, argsArray);
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldfld, fakeField);
            ilGenerator.Emit(OpCodes.Ldstr, method.Name);
            ilGenerator.Emit(OpCodes.Ldloc_0);

            if (method.ReturnType == typeof(void))
            {
                ilGenerator.Emit(OpCodes.Callvirt, typeof(Fake).GetMethod(nameof(Fake.HandleVoidCall)));
            }
            else
            {
                ilGenerator.Emit(OpCodes.Callvirt, typeof(Fake).GetMethod(nameof(Fake.HandleReturnCall)));
                if (method.ReturnType.IsValueType) ilGenerator.Emit(OpCodes.Unbox_Any, method.ReturnType);
            }

            ilGenerator.Emit(OpCodes.Ret);

            typeBuilder.DefineMethodOverride(methodBuilder, method);
        }
    }
}
