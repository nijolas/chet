﻿namespace Chet
{
    public class NotNullConstraint : Constraint
    {
        internal override bool Matches(object arg)
        {
            return (arg != null);
        }
    }
}
