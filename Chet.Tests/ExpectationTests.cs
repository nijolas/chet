﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Chet.Tests
{
    [TestClass]
    public class ExpectationTests
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ObjIsNotAFake_ShouldThrow()
        {
            var failed = new NotFake().Stub(x => x.DoStuffNoArgs());
        }

        [TestMethod]
        public void ObjIsAFake_ShouldNotThrow()
        {
            var fake = Faker.Cover<IFakeTest>();
            var succeeded = fake.Stub(x => x.DoStuffNoArgs());
        }

        [TestMethod]
        public void Returns_SetsMethodReturnValue()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Stub(x => x.ReturnStuffNoArgs())
                .Returns("test");

            // act
            var result = fake.ReturnStuffNoArgs();

            // assert
            Assert.AreEqual("test", result);
        }

        [TestMethod]
        public void Repeat_One_SucceedsOnOneCall()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(1);

            // act
            fake.DoStuffNoArgs();

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Repeat_Two_SucceedsOnTwoCalls()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(2);

            // act
            fake.DoStuffNoArgs();
            fake.DoStuffNoArgs();

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Repeat_Two_FailsOnThreeCalls()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(2);

            // act
            fake.DoStuffNoArgs();
            fake.DoStuffNoArgs();
            fake.DoStuffNoArgs();

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Repeat_One_FailsOnNoCall()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(1);

            // act
            //fake.DoStuffNoArgs();

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Repeat_One_FailsOnTwoCalls()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(1);

            // act
            fake.DoStuffNoArgs();
            fake.DoStuffNoArgs();

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void Repeat_Two_FailsOnOneCall()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(2);

            // act
            fake.DoStuffNoArgs();

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Repeat_Random_SucceedsOnExactNumber()
        {
            // arrange
            int n = new Random().Next(100);

            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs())
                .Repeat(n);

            // act
            for (int i = 0; i < n; i++)
            {
                fake.DoStuffNoArgs();
            }

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void Repeat_NotCalled_AcceptsAnyNumberOfCalls()
        {
            // arrange
            int n = new Random().Next(100);

            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs());

            // act
            for (int i = 0; i < n; i++)
            {
                fake.DoStuffNoArgs();
            }

            // assert
            fake.VerifyExpectations();
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void Throws_MethodThrows()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs()).Throws(new DivideByZeroException());

            // act
            fake.DoStuffNoArgs();
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectationFailedException))]
        public void IsNotCalled_MethodIsCalled_Throws()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs()).IsNotCalled();

            // act
            fake.DoStuffNoArgs();
        }

        [TestMethod]
        public void IsNotCalled_MethodIsNotCalled_Succeeds()
        {
            // arrange
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.DoStuffNoArgs()).IsNotCalled();

            // act
            fake.VerifyExpectations();
        }

        [TestMethod]
        public void MultipleExpectations_ReturnInOrder()
        {
            var fake = Faker.Cover<IFakeTest>();
            fake.Expect(x => x.ReturnStuffNoArgs()).Returns("first").Repeat(1);
            fake.Expect(x => x.ReturnStuffNoArgs()).Returns("second and third").Repeat(2);
            fake.Expect(x => x.ReturnStuffNoArgs()).Returns("fourth").Repeat(1);

            // act
            Assert.AreEqual("first", fake.ReturnStuffNoArgs());
            Assert.AreEqual("second and third", fake.ReturnStuffNoArgs());
            Assert.AreEqual("second and third", fake.ReturnStuffNoArgs());
            Assert.AreEqual("fourth", fake.ReturnStuffNoArgs());
        }

        class NotFake : IFakeTest
        {
            public void DoStuffNoArgs()
            {
                // do nothing
            }

            public void DoStuffWithArgs(string arg1)
            {
                // do nothing
            }

            public string ReturnStuffWithArgs(string arg1)
            {
                return null;
            }

            public string ReturnStuffNoArgs()
            {
                return null;
            }

            public void Int(int i)
            {
                // do nothing
            }

            public void Decimal(decimal d)
            {
                // do nothing
            }

            public void String(string s)
            {
                // do nothing
            }

            public void Long(long l)
            {
                // do nothing
            }
        }
    }
}
